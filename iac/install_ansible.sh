#!/bin/bash
type -P curl > /dev/null || { echo >&2 "I require curl but it's not installed.  Aborting."; exit 1; }
TAGS=`curl -s https://github.com/ansible/ansible/tags|grep -Eoi '<a [^>]+>'|grep -Eo '[^v\"]+'|grep -E "([0-9]{1,}\.)+[0-9]{1,}"|sed 's/.tar.gz//g;s/.zip//g'|sort -ur`

type -P pip  > /dev/null || { echo >&2 "I require pip but it's not installed.  Aborting."; exit 1; }
type -P ansible > /dev/null && { echo >&2 "ansible is already installed.  Aborting."; exit 1;  }

_download() {
	LATEST=`echo "$TAGS"|grep -m1 ""`
	VERSION="$LATEST"
	sudo pip install ansible=="$VERSION"
}

case "$1" in
	ls)
	echo "$TAGS"
	;;
	*)
	_download
esac
