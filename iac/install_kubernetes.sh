#!/bin/bash
set -x
OS=$(uname | tr [:upper:] [:lower:])

if [ ! -d $HOME/bin ]; then
	mkdir -p $HOME/bin
fi
RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"

if [ $OS == linux ]; then
	cd $HOME/bin
	curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/{kubeadm,kubelet,kubectl}
	chmod +x {kubeadm,kubelet,kubectl}
	
	VERSION="v1.11.1"
	curl -L https://github.com/kubernetes-incubator/cri-tools/releases/download/$VERSION/crictl-$VERSION-linux-amd64.tar.gz | tar xzv
	mv crictl $HOME/bin

	
	if [ -L "/sbin/init" ]; then
		curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/kubelet.service" | sed "s:/usr/bin:$HOME/bin:g" > /etc/systemd/system/kubelet.service
		mkdir -p /etc/systemd/system/kubelet.service.d
		curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/10-kubeadm.conf" | sed "s:/usr/bin:$HOME/bin:g" > /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
	fi
	
else
	curl -L https://storage.googleapis.com/kubernetes-helm/helm-v2.9.1-$OS-amd64.tar.gz | tar xzv
	mv $OS-amd64/helm $HOME/bin/
	rm -rf $OS-amd64
	chmod +x $HOME/bin/helm


	curl -o $HOME/bin/cfssl https://pkg.cfssl.org/R1.2/cfssl_darwin-amd64
	curl -o $HOME/bin/cfssljson https://pkg.cfssl.org/R1.2/cfssljson_darwin-amd64
	chmod +x $HOME/bin/{cfssl,cfssljson}
	curl -L https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/$OS/amd64/kubectl -o $HOME/bin/kubectl
	chmod +x $HOME/bin/kubectl

	curl -L https://github.com/kubernetes/minikube/releases/download/v0.28.2/minikube-$OS-amd64 -o $HOME/bin/minikube
	chmod +x $HOME/bin/minikube

	curl -L https://github.com/jenkins-x/jx/releases/download/v1.3.133/jx-$OS-amd64.tar.gz | tar xzv
	mv jx $HOME/bin/
fi
