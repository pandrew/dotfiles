#!/bin/bash

type -P go > /dev/null || { echo >&2 "I require go but it's not installed.  Aborting."; exit 1; }
type -P make > /dev/null || { echo >&2 "I require make but it's not installed.  Aborting."; exit 1; }
type -P curl > /dev/null || { echo >&2 "I require curl but it's not installed.  Aborting."; exit 1; }

export DEFAULT_COMMIT=26ae9a0246d4acbcd09d6b8e0d00896f3eed6925

pre_clean() {
	if [ -d $HOME/gopath/src/github.com/linuxkit/linuxkit ]; then
		rm -rvf $HOME/gopath/src/github.com/linuxkit/linuxkit
	fi
}

show_commits() {
	curl -s https://api.github.com/repos/linuxkit/linuxkit/commits?per_page=10|jq  '.[]|{sha: .sha, message: .commit.message}'
	exit 0
}

install_pkg() {
	mkdir -p $HOME/gopath/src/github.com/linuxkit/linuxkit
	cd $HOME/gopath/src/github.com/linuxkit \
		&& git clone $GIT_OPT -n https://github.com/linuxkit/linuxkit.git \
		&& cd linuxkit \
		&& git checkout "$COMMIT"

	cd $HOME/gopath/src/github.com/linuxkit/linuxkit \
		&& make local-static \
		&& make PREFIX=$HOME install	

}

case "$1" in
	ls)
		show_commits
		;;
	test)
		export LATEST=`curl -H "Accept: application/vnd.github.VERSION.sha" \
			https://api.github.com/repos/linuxkit/linuxkit/commits/master`
		export COMMIT="$LATEST"
		export GIT_OPT="--depth=1"
		pre_clean
		install_pkg
		;;
	*)	
		export OPT=$1
		export COMMIT=${1:-$DEFAULT_COMMIT}
		pre_clean
		install_pkg
		;;
esac
