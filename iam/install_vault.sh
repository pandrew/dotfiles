#!/bin/bash
LATEST=`curl -s https://releases.hashicorp.com/vault/ |grep -Eoi '<a [^>]+'|sed -n 2p|grep -Eo "([0-9]{1,}\.)+[0-9]{1,}"`
VERSION=${LATEST:-0.10.2}

OS=$(uname | tr [:upper:] [:lower:])
URI=https://releases.hashicorp.com/vault/${VERSION}/vault_${VERSION}_${OS}_amd64.zip

_download() {
        if [ ! -d $HOME/bin ]; then
                mkdir $HOME/bin
        fi
        cd $HOME/bin \
                && curl -fL -O $URI \
                && unzip vault_${VERSION}_${OS}_amd64.zip \
                && rm vault_${VERSION}_${OS}_amd64.zip
}
_download
