#!/bin/bash

jenkins() {
	docker run -it --privileged -v /usr/local/bin/docker:/usr/local/bin/docker -v /var/run/docker.sock:/var/run/docker.sock --net=host --group-add=users --name=jenkins jenkins
}

"$@"
