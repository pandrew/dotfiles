#!/bin/bash


BASENAME="$(basename $0)"
TRAPFILE="$HOME/$BASENAME.lock"
if { set -C; 2>/dev/null >$TRAPFILE; }; then
	echo "setting trap to $TRAPFILE"
	trap "$TRAPFILE" EXIT
else
	 echo "Lock file "$TRAPFILE" exists… exiting"
	 exit
fi


if [ -d /media/paul/data1 ]; then
	restic -r rclone:gdrive:/restic/data1 backup /media/"$USER"/data1
fi


restic -r rclone:gdrive:/restic/"$HOSTNAME"/"$USER" backup -e $HOME/.dbus -e $HOME/.kube ~
